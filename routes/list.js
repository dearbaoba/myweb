var express = require('express');
var router = express.Router();
var DBTools = require('../modules/dbTools');
var user = require('../modules/userTools');


router.get('/', function(req, res, next) {
    user.verify(req, res, function(data) {
        DBTools.select("my_table", req.query.page, req.query.count, function(data){
            res.render('list', {elements: data});
        });
    });
});

module.exports = router;
