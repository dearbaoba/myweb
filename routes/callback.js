var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {
    res.cookie('name', req.query.name);
    res.cookie('pass', req.query.pass);
    res.redirect('index.html');
});

module.exports = router;