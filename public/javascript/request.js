
Request = function() {
    return {
        bind: function(id, keyword){
            $.ajax({
                url: keyword,
                method:"GET",
            }).done(function(data) {
                $(id).html(data);
            });
        }
    }
}

var request = new Request();