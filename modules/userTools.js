var request = require('request');

var verify = function(req, res, callback) {
    var name = req.cookies['name'];
    var pass = req.cookies['pass'];
    var url = "http://"+name+":"+pass+"@192.168.1.103:8082/verify"
    request({url: url}, function (error, response, body) {
        if(error || body == 'Unauthorized') {
            res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
            return res.send(401);
        } else {
            callback(body);
        }
    });
}


module.exports = {
    "verify": verify, 
};