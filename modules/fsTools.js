var fs = require('fs');

var save_file = function(file, template, obj, callback) {
    fs.readFile(template, function(err, data) {
        var template = String(data);
        for(var item in obj) {
            var re = new RegExp("#"+item+"#", "g");
            template = template.replace(re, obj[item]);
        }
        fs.writeFile(file, template, function(err) {
            callback();
        })
    })
}

module.exports = {
    "save_file": save_file, 
};