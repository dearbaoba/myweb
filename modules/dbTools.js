var mysql = require("mysql");

var DB_conn = function(callback) {
    var con = mysql.createConnection({
        host: "192.168.1.103",
        port:"8081",
        user: "root",
        password: "qw1717",
        database: "myDB"
    });

    con.connect(function(err) {
        callback(con);
    });
}

var DB_exec = function(con, sql, callback) {
    console.log(sql);
    con.query(sql,
        function(err, data) {
            callback(data);
    });
}

var DB_close = function(con, callback) {
    con.end(function(err) {
        // The connection is terminated gracefully
        // Ensures all previously enqueued queries are still
        // before sending a COM_QUIT packet to the MySQL server.
        callback();
    });
}

var DB_query = function(table, sql_func, callback) {
    DB_conn(function(con) {
        DB_exec(con, sql_func(), function(data) {
            DB_close(con, function() {
                console.log(data);
                callback(data);
            });            
        });
    });
}

// interface

var select = function(table, page, count, callback) {
    DB_query(table, function() {
        return "SELECT * FROM "+table+" order by ID desc limit "+ page * count +", "+count;
    }, callback);
}

var search = function(table, obj, callback) {
    DB_query(table, function() {
        var key_value = [];
        for(var item in obj) {
            key_value.push(item+"="+"'"+obj[item]+"'");
        }
        return "SELECT * FROM "+table+" WHERE "+key_value.join(" AND ");
    }, callback);
}

var alter = function(table, obj, key, callback) {
    DB_query(table, function() {
        var alter_value = [];
        var key_value = [];
        for(var item in obj) {
            alter_value.push(item+"="+"'"+obj[item]+"'");
        }
        for(var item in key) {
            if(item == 'r') continue;
            key_value.push(item+"="+"'"+key[item]+"'");
        }
        return "UPDATE "+table+" SET "+alter_value.join(", ")+" WHERE "+key_value.join(" AND ");
    }, callback);
}

var insert = function(table, obj, callback) {
    DB_query(table, function() {
        var keys = [];
        var values = [];
        for(var item in obj) {
            keys.push(item);
            if(typeof obj[item] == "string") {
                values.push("'"+obj[item]+"'");
            } else {
                values.push(obj[item]);
            }
        }
        return "INSERT INTO "+table+"("+keys.join(",")+") VALUES("+values.join(",")+")";
    }, callback);
}

module.exports = {
    "select": select, 
    "insert": insert,
    "search": search,
    "alter": alter,
};
